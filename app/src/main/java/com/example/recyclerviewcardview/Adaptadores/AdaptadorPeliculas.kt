package com.example.recyclerviewcardview.Adaptadores

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewcardview.Actividades.Detalles
import com.example.recyclerviewcardview.Actividades.VisorImagen
import com.example.recyclerviewcardview.ClaseDatos.Pelicula
import com.example.recyclerviewcardview.R
import kotlinx.android.synthetic.main.elemento_lista_pelicula.view.*

class AdaptadorPeliculas(private var lista: ArrayList<Pelicula>, private var contexto: Context):RecyclerView.Adapter<AdaptadorPeliculas.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.elemento_lista_pelicula,
                parent,
                false
            ),
            contexto
        )
    }

    override fun getItemCount(): Int {
        return lista.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(lista[position])
    }

    class ViewHolder(var vista:View, var contexto:Context): RecyclerView.ViewHolder(vista){
        fun bind(pelicula: Pelicula){
            vista.iv_Poster.setImageResource(pelicula.id_Imagen)
            vista.txt_Titulo.text = pelicula.titulo
            vista.txt_Genero.text = pelicula.genero
            vista.rb_Calificacion.rating = pelicula.calificacion.toFloat()

            vista.iv_Poster.setOnClickListener{
                contexto.startActivity(Intent(contexto, VisorImagen::class.java).putExtra("pel", pelicula))
            }

            vista.setOnClickListener{
                contexto.startActivity(Intent(contexto, Detalles::class.java).putExtra("pel", pelicula))
            }

        }
    }

}