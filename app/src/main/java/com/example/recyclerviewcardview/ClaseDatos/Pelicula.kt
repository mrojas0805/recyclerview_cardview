package com.example.recyclerviewcardview.ClaseDatos

import java.io.Serializable

data class Pelicula (
    var id_Imagen:Int,
    var titulo:String,
    var director:String,
    var genero:String,
    var calificacion:Double,
    var duracion:Int,
    var fecha:String
):Serializable