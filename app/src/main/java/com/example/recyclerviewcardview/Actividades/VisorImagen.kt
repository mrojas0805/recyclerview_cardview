package com.example.recyclerviewcardview.Actividades

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.recyclerviewcardview.ClaseDatos.Pelicula
import com.example.recyclerviewcardview.R
import kotlinx.android.synthetic.main.activity_visor_imagen.*

class VisorImagen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_visor_imagen)

        val pelicula = intent.getSerializableExtra("pel") as Pelicula

        ivPelicula.setImageResource(pelicula.id_Imagen)
    }
}