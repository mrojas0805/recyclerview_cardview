package com.example.recyclerviewcardview.Actividades

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.recyclerviewcardview.ClaseDatos.Pelicula
import com.example.recyclerviewcardview.R
import kotlinx.android.synthetic.main.activity_detalles.*

class Detalles : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalles)

        val pelicula = intent.getSerializableExtra("pel") as Pelicula

        txt_Titulo.text = getString(R.string.titulo, pelicula.titulo)
        txt_Director.text = getString(R.string.director, pelicula.director)
        txt_Genero.text = getString(R.string.genero, pelicula.genero)
        txt_Calificacion.text = getString(R.string.calificacion, pelicula.calificacion.toString())
        txt_Duracion.text = getString(R.string.duracion, pelicula.duracion.toString())
        txt_Fecha.text = getString(R.string.fecha, pelicula.fecha)
    }
}