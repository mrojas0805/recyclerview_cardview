package com.example.recyclerviewcardview.Actividades

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recyclerviewcardview.Adaptadores.AdaptadorPeliculas
import com.example.recyclerviewcardview.ClaseDatos.Pelicula
import com.example.recyclerviewcardview.R
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(){

    lateinit var toolbar: Toolbar
    lateinit var drawerLayout: DrawerLayout
    lateinit var navView: NavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rv_Pelicula.layoutManager = LinearLayoutManager(this)
        rv_Pelicula.adapter =
            AdaptadorPeliculas(
                generarDatosPrueba(),
                this
            )

    }

    private fun generarDatosPrueba():ArrayList<Pelicula>{
        val lista = ArrayList<Pelicula>()
        lista.add(
            Pelicula(
                R.drawable.interestelar,
                "Interestelar",
                "Christopher Nolan",
                "Ciencia Ficción",
                4.3,
                169,
                "2014"
            )
        )
        lista.add(
            Pelicula(
                R.drawable.forma_agua,
                "La Forma del Agua",
                "Guillermo del Toro",
                "Cine Fantástico",
                3.65,
                123,
                "2017"
            )
        )
        lista.add(
            Pelicula(
                R.drawable.extraordinario,
                "Extraordinario",
                "Steven Chboaky",
                "Drama",
                4.0,
                113,
                "2017"
            )
        )
        lista.add(
            Pelicula(
                R.drawable.la_llegada,
                "La Llegada",
                "Denis Villeneuve",
                "Ciencia Ficción",
                3.95,
                116,
                "2016"
            )
        )
        lista.add(
            Pelicula(
                R.drawable.exmachina,
                "Ex-Maquina",
                "Alex Garland",
                "Ciencia Ficción",
                3.85,
                108,
                "2015"
            )
        )
        lista.add(
            Pelicula(
                R.drawable.jumanji,
                "Jumanji: En la Selva",
                "Jake Kaadan",
                "Acción",
                3.5,
                119,
                "2017"
            )
        )

        return lista
    }
}